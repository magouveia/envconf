# envconf

Go package for load and set environment variables from .env text files.


## Installation

Needed third party package:
```shell
go get -u github.com/joho/godotenv
```

As a library in Go workspace:
```shell
go get -u gitlab.com/magouveia/envconf
```


## Overview

Load values defined in .env files and create environment variables at Operating System level.

Environment variables created along the Go code will only be acessible inside the Go project.


## File: config.env

This file is always loaded, if it exists.

In this file there must exist a variable named as ENVIRONMENT.

The value of ENVIRONMENT can be: development, test, production.

If the value of ENVIRONMENT is different of the above ones or it is not defined, it will be: development.

The value of ENVIRONMENT is case insensitive.


## File: config_xxx.env

Depending on the value of ENVIRONMENT, the corresponding file config_xxx.env will be loaded, if it exists.

xxx = development, test, production


## Precedence of multiple values


If some variable is defined more than once, the value precedence is:

1) existing O.S. environment variable, defined at O.S. startup.

2) value defined in: config.env

3) value defined in: config_xxx.env


## Usage example


On terminal, create an enviroment variable before the test:
```shell
$ EXTERNAL_VAR="Variable defined before the Go test"
$ export EXTERNAL_VAR
$ echo $EXTERNAL_VAR	# test if it was correctly created
Variable defined before the Go test
```
We are interested in observe that this external variable will not be affected by the envconf execution.

In go workspace, create the directories and files like the following structure:
```shell
src/
 |_yourtest/
    |_ main.go:
    |_ config.env
    |_ config_development.env
    |_ config_test.env
    |_ config_production.env
```

File main.go
```go
package main

import (
	"fmt"
	"os"
	"gitlab.com/magouveia/envconf"
)

func main() {
	fmt.Println("Environment variables before config:")
	printEnvironment()

	envconf.LoadEnvironmentVariables()

	fmt.Println("\nEnvironment variables after config:")
	printEnvironment()
}

func printEnvironment() {
	fmt.Println("ENVIRONMENT :", os.Getenv("ENVIRONMENT"))
	fmt.Println("SYSTEM_TITLE :", os.Getenv("SYSTEM_TITLE"))
	fmt.Println("SPECIFIC_VAR :", os.Getenv("SPECIFIC_VAR"))
	fmt.Println("EXTERNAL_VAR :", os.Getenv("EXTERNAL_VAR"))
}
```

File config.env
```shell
ENVIRONMENT=production
SYSTEM_TITLE="Environment variables with package envconfig"
EXTERNAL_VAR="Redefining the external variable"
```

File config_development.env
```shell
SPECIFIC_VAR="Specific variable at DEVELOPMENT config file"
```

File config_test.env
```shell
SPECIFIC_VAR="Specific variable at TEST config file"
```

File config_production.env
```shell
SPECIFIC_VAR="Specific variable at PRODUCTION config file"
```

Execution and results:
```shell
$ go run main.go 
Environment variables before config:
ENVIRONMENT : 
SYSTEM_TITLE : 
SPECIFIC_VAR : 
EXTERNAL_VAR : Variable defined before the Go test

Environment variables after config:
ENVIRONMENT : production
SYSTEM_TITLE : Environment variables with package envconfig
SPECIFIC_VAR : Specific variable at PRODUCTION config file
EXTERNAL_VAR : Variable defined before the Go test
```


## Comments about the results

As we can see, the files config_development.env and config_test.env were not loaded because in file config.env the value of ENVIRONMENT is "production".

The existing environment variable (EXTERNAL_VAR) created before the test was not changed, as desired.


## Thanks

Thank you for your interest and I hope envconf package can be useful for you!

If you have suggestions, they will be welcome.