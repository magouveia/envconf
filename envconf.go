package envconf

import (
	"os"
	"strings"

	"github.com/joho/godotenv"
)

const (
	environment = "ENVIRONMENT"
	development = "development"
	test        = "test"
	production  = "production"
	configStd   = "config.env"
	configDev   = "config_development.env"
	configTest  = "config_test.env"
	configProd  = "config_production.env"
)

// LoadEnvironmentVariables loads the .env configuration files
// and creates the corresponding environment variables at O.S. level.
func LoadEnvironmentVariables() {
	loadStandardEnvVars()
	loadSpecificEnvVars()
}

func loadStandardEnvVars() {
	godotenv.Load(configStd)
	// store value of ENVIRONMENT in lowercase
	os.Setenv(environment, strings.ToLower(os.Getenv(environment)))
}

func loadSpecificEnvVars() {
	// If env var ENVIRONMENT doesn't exist or is empty, redefine it as: development
	value, found := os.LookupEnv(environment)
	if !found || value == "" {
		os.Setenv(environment, development)
	}

	switch os.Getenv(environment) {
	case development:
		godotenv.Load(configDev)
	case test:
		godotenv.Load(configTest)
	case production:
		godotenv.Load(configProd)
	default:
		godotenv.Load(configDev)
	}
}
