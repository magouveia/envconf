module gitlab.com/magouveia/envconf

go 1.12

require github.com/joho/godotenv v1.3.0
